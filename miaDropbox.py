#!/usr/bin/env python
#-------------------------------------------------------------------------------
# Name:        miadropbox interface for dropbox api
#              
# Purpose:     System to record video of movement for use with security camera
#              
#
# Author:      Dani Thomas
#
# Requires:    json,dropbox
# Based on:    
#-------------------------------------------------------------------------------
import mialogging
import miaprocesses
import dropbox
import glob
import os
import time
import json
import subprocess
from datetime import datetime
from dateutil import parser

NOTHING=0
UPLOAD=1
DOWNLOAD=2

class miadropbox:
  client=None
  
  # Connect to the dropbox account
  def __init__(self, access_token):
    try:
      self.client = dropbox.Dropbox(access_token)
      logging.info("[SUCCESS] dropbox account linked")
    except:
      self.client=None
      logging.error("Error connecting" + sys.exc_info()[0])
      
  def checkConnected(self):
    if self.client==None:
      return False
    else:
      return True
  
  # Get time difference in minutes so we don't move something that is still been written to  
  def getTimeDifferenceFromNow(self, TimeStart, TimeEnd):
    timeDiff = TimeEnd - TimeStart
    return timeDiff.seconds / 60

  # check if the file already exists so we don't send it again
  def existsAlready(self,path,folder_metadata):
    if folder_metadata.entries<>None:
      for entry in folder_metadata.entries:
        if path == entry.path_display:
          return True       
    return False
  
  # saves the dropbox version file - so we know if there are differences
  def saveVersion(self, version,versionFile):
    wf=open(versionFile,'wb')
    json.dump(version, wf)
    wf.close()

  # upload the file from the local path to the dropbox path
  def uploadFile(self, localFullPath, upFile):
    f=open(localFullPath,'rb')
    try:
      response=self.client.files_upload(f,upFile,dropbox.files.WriteMode.overwrite)
    except dropbox.exceptions.ApiError as e:
      logging.error("dropbox error upload file {0}".format(e.status))
      return
  
  # This syncs files between local path and dropbox
  def syncFile(self,localPath, upFile,currentDirectory):
    try:
      dropConfigFile= self.client.files_get_metadata("/" + upFile) 
      dropRevision = dropConfigFile.rev
      dmtime_dt = dropConfigFile.client_modified

    except dropbox.exceptions.ApiError as err:
      dmtime_dt=parser.parse('1900-01-01 01:00')
    
    versionFile = currentDirectory + upFile + '.ver'
    localFullPath=localPath + upFile
    mtime = os.path.getmtime(localFullPath)
    mtime_dt = datetime(*time.gmtime(mtime)[:6])
    
    # if the date in dropbox is the same as locally do nothing
    if mtime_dt == dmtime_dt:
      logging.debug("files are the same")
      return NOTHING
    # if local file is newer upload it
    elif mtime_dt > dmtime_dt:
      f=open(localFullPath,'rb')
      try:
        response=self.client.files_upload(f,"/"+upFile,dropbox.files.WriteMode.overwrite, client_modified=mtime_dt)
      except dropbox.exceptions.ApiError as e:
        logging.error("dropbox error syncFile {0}".format(e.status))
        return
      # save a copy of the version file to compare next time
      versionJson=json.loads('{"revision":"' + response.rev + '"}')
      self.saveVersion(versionJson,versionFile)
      logging.info("uploaded file " + upFile)
      return UPLOAD
    else:
      # Download if revisions are different. We can't just go on time as time file uploaded will be after the local file time
      try:
        f=open(versionFile,'rb')
        localVersion=json.load(f)
        localRevision=localVersion['revision']
      except IOError:
        localRevision=-1
        
      if dropRevision==localRevision:
        logging.debug("file are same revision")
        return NOTHING
      else:
        metadata,f = self.client.files_download("/" + upFile)
        out = open(localFullPath, 'wb')
        out.write(f.content)
        out.close()
        versionJson=json.loads(r'{"revision":"' + metadata.rev + r'"}')
        self.saveVersion(versionJson,versionFile)        
        dmtime_dt = metadata.server_modified
        utime = time.mktime(dmtime_dt.timetuple())
        os.utime(localFullPath,(utime,utime))
        logging.info("downloaded file " + str(upFile))
        return DOWNLOAD
    
  # Upload file to folder creating if necessary
  def uploadToFolder(self,localPath,dropFolder,filePattern):
    # Get all files in the local folder
    upFiles = glob.glob(localPath+dropFolder+'/'+filePattern)

    try: #If folder not there catch exception and create
      folder_metadata=self.client.files_list_folder('/'+dropFolder)
    except dropbox.exceptions.ApiError as e:
      logging.error("dropbox error getting folder {0}".format(dropFolder))
      try:
        folder_metadata=self.client.files_create_folder('/'+dropFolder)
      except dropbox.exceptions.ApiError as e:
        logging.error("dropbox error creating folder {0}".format(dropFolder))
        return
    
    # Upload files if they don't exist in the dropbox folder already  
    for upFile in upFiles:
      f = open(upFile, 'rb')

      dbFile=upFile[len(localPath)-1:]
      
      if not(self.existsAlready(dbFile,folder_metadata)):
        crDate=os.path.getctime(upFile)
        TimeStart=datetime.fromtimestamp(crDate)
        TimeEnd=datetime.now()
        logging.debug(dbFile + ' Start: ' + str(TimeStart) + ' End: ' + str(TimeEnd))
        timeDiff = self.getTimeDifferenceFromNow(TimeStart,TimeEnd)

        # Allow five minutes in case file is still being written to
        if timeDiff > 5:
          logging.info("transferring {0}".format(dbFile))
          self.client.files_upload(f,dbFile)

if __name__ == '__main__':

  # Get the directory this script comes from
  def getCurrentDirectory():
    return os.path.dirname(os.path.realpath(__file__))+'/'
    
  # Get and load Json file
  def tryToLoadJsonFile(jsonFilePath):  
    if os.path.exists( jsonFilePath ):
        try:
            with open( jsonFilePath ) as jsonFile:
                
                return json.load( jsonFile )
                           
        except Exception as e:
            logging.error( "Unable to load file. Exception was " + str( e ) ) 
    else:
        logging.error( "Unable to locate file: " + jsonFilePath)
  
  # Set up to log to common logger
  logging=mialogging.logger
  
  # We don't wanna be running if we aready are
  miaprocess=miaprocesses.MiaProcess() 
  if miaprocess.amIrunning(__file__):
    logging.info("Already running " + str(__file__))
    exit(0)
  
  # Get global config file for paths etc      
  HOME_DIR=getCurrentDirectory()
  GlobalConfigFile=tryToLoadJsonFile(HOME_DIR + 'globalConfig.json')

  LOG_FILE=str(GlobalConfigFile['LOG_FILE'])
  REMOTE_CONFIG_FILE = str(GlobalConfigFile['REMOTE_CONFIG_FILE'])
  VIDEO_ROOT=str(GlobalConfigFile['VIDEO_ROOT'])
  
  # Get today's folder
  start=datetime.now()
  dropFolder=str.format(str(GlobalConfigFile['VIDEO_DIR']),start.strftime("%Y-%m-%d"))
  filePattern=str.format(str(GlobalConfigFile['VIDEO_NAME']),"*")
  
  # Use key to access the dropbox account
  accessKey=str(GlobalConfigFile['ACCESS_KEY'])
  mDropbox= miadropbox(accessKey)
  
  if mDropbox.checkConnected():
    # Upload all files that match filePattern to the dropbox folder
    mDropbox.uploadToFolder(VIDEO_ROOT,dropFolder,filePattern)
    ret=mDropbox.syncFile(VIDEO_ROOT,REMOTE_CONFIG_FILE,HOME_DIR)
    retM=mDropbox.syncFile(VIDEO_ROOT,str(GlobalConfigFile['MASKED_FILE']),HOME_DIR)
    
    # if the config file has been updated then we need to restart the motion detect to get the new values  
    if ret==DOWNLOAD or retM==DOWNLOAD:
      logging.info("Config Updated")
      subprocess.call(["/etc/init.d/motionDetect", "stop"])
      time.sleep(20)
      subprocess.call(["/etc/init.d/motionDetect", "start"])
      time.sleep(20)
    
    # Upload log file
    mDropbox.uploadFile(VIDEO_ROOT+LOG_FILE,"/"+LOG_FILE+".txt")
    