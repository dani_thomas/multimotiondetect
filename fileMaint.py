#-------------------------------------------------------------------------------
# Name:        fileMaint
#              
# Purpose:     This is a simple script which removes old directories once the date
#              has gone past a certain days
#
# Author:      Dani Thomas
#
# Requires:    
# Based on:    
#-------------------------------------------------------------------------------
import os
import shutil
from datetime import datetime,timedelta,date
import mialogging
import miaprocesses
import json

NUMBER_OF_DAYS_TO_KEEP=2

start=date.today()

diskSpaceToReserve = 40 * 1024 * 1024 # Keep 40 mb free on disk


# Keep free space above given level
def removeOldFolders(daysAgo,DirectoryPath,dirPattern):
  PDaysAgo = start - timedelta(days=daysAgo)
  dirnames= sorted(os.listdir(DirectoryPath))

  for dirname in dirnames:

    if dirname.startswith(dirPattern):
        if datetime.strptime(dirname[len(dirPattern):],"%Y-%m-%d").date() < PDaysAgo:
          shutil.rmtree(os.path.join(DirectoryPath,dirname))
          logging.info( "Deleting {0}".format(os.path.join(DirectoryPath,dirname)))

# not used but might be worth if for someone
def getFreeSpace():
  st = os.statvfs(".")
  du = st.f_bavail * st.f_frsize
  print str(du)
  return du

# Get the directory this script comes from
def getCurrentDirectory():
  return os.path.dirname(os.path.realpath(__file__))+'/'
  
# Get and load Json file
def tryToLoadJsonFile(jsonFilePath):  
  if os.path.exists( jsonFilePath ):
      try:
          with open( jsonFilePath ) as jsonFile:
              
              return json.load( jsonFile )
                         
      except Exception as e:
          logging.error( "Unable to load file. Exception was " + str( e ) ) 
  else:
      logging.error( "Unable to locate file: " + jsonFilePath)
  
# Set up to log to common logger
logging=mialogging.logger
  
# We don't wanna be running if we aready are
miaprocess=miaprocesses.MiaProcess() 
if miaprocess.amIrunning(__file__):
  logging.info("Already running " + str(__file__))
  exit(0)

logging.info ("File maint running")
  
# Get global config file for paths etc      
HOME_DIR=getCurrentDirectory()
GlobalConfigFile=tryToLoadJsonFile(HOME_DIR + 'globalConfig.json')
VIDEO_ROOT=str(GlobalConfigFile['VIDEO_ROOT'])
filePattern=str.format(str(GlobalConfigFile['VIDEO_DIR']),"")

removeOldFolders(NUMBER_OF_DAYS_TO_KEEP,VIDEO_ROOT,filePattern)