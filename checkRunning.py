#-------------------------------------------------------------------------------
# Name:        checkRunning
#              
# Purpose:     Checks that the motion detect system is running and will restart
#              if not
#
# Author:      Dani Thomas
#
# Requires:    
# Based on:    
#-------------------------------------------------------------------------------
import time
import mialogging
import miaprocesses
import subprocess

if __name__ == '__main__':
  logging=mialogging.logger
  miaprocess=miaprocesses.MiaProcess()
  
  if miaprocess.amIrunning(__file__):
    logging.info("Already running " + str(__file__))
    exit(0)
  
  # Check whether system is running
  instances=miaprocess.getProcessesRunning('multiMotionDetect.py')
  
  # multiMotionDetect is a 3-process system
  if instances == 3:
    logging.info("Motion Detect ok")
  else:
      # if not will need to restart
      logging.info("Motion Detect isn't running correctly. Stopping instances:" + str(instances))
      subprocess.call(["/etc/init.d/motionDetect", "stop"])
      time.sleep(20)
      instances=miaprocess.getProcessesRunning('multiMotionDetect.py')
      if instances==0:
        logging.info("All instances stopped:" + str(instances))
        subprocess.call(["/etc/init.d/motionDetect", "start"])
      else:
        #looks like instances are stuck
        logging.info("Instances stuck:" + str(instances))