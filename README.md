The main part of this system is multiMotionDetect.py. It uses a lot of the multiprocessing queues and events.

First of all you need to decide where you want the video images stored MotionVideos and set this value in the globalConfig.json file. Then copy the config.json.txt and maskedAreas.json.txt to the root of this folder. The config.json.txt has the following setting which can be edited remotely.

{
  "frameThreshold": "4",
  "staticThreshold":"100",
  "min_area":"650",
  "postSeconds":"7",
  "readCamNice":"-6", 
  "checkMotionNice":"5",
  "writeCamNice":"5",
  "maxqsize":"6"
}

FrameThreshold: is the number of significant frames before motion is detected.
staticThreshold: is the number of static frames before we turn filming off.
minArea: is the minimum size of the area in order to be counted as significant.
postSeconds: This is the number of seconds from the end of filming for the movement to go through the queue.
readCamNice: This is how much priority to be given to the readCam process. This is between -20 and +20 (the lower the figure the higher the priority). But don't overdo it or you will crash the operating system.
checkMotionNice: The priority for the motion detect process.
writeCamNice: The priority of the camera writing process.
maxqsize: This is the number of seconds which is then multiplied by the frames per second.

I mostly only change the min_area to account for wind conditions.

If you would rather use a simple logger rather than the socket logger (below) change the import miaLogging to

import logging
logging.basicConfig(filename='example.log',level=logging.DEBUG)

and remove the log receiver from the motionDetect file and everything else should work fine.

If you want to run the motion detect automatically on startup.

First edit the script and check that the homedir points to where you have multiMotionDetect.py, then copy the motionDetect file to /etc/init.d ie

cp motionDetect /etc/init.d/motionDetect

Should be executable already but

chmod +x /etc/init.d/motionDetect

Finally register the script with

sudo update-rc.d motionDetect defaults

You can also start, stop and restart the system with

sudo /etc/init.d/motionDetect start|stop|restart

By default the miaLogReceiver socket logging will start at the same time. The other three programs are independent but use the same socket logger (but could easily be converted). I call all these using a cron script of different intervals. For instructions look here.

CheckRunning.py checks that multiMotionDetect.py is running and does a restart if not.

fileMaint.py does housekeeping on the video folders removing these after the given number of days. It removes subdirectories of the motion video folder set in the first paragraph. It checks that they start with "MV" so make sure you haven't got another directory of importance starting with the same characters within that folder.

Finally if you want to view your videos, logs and config files remotely then you will need to set up dropbox.

First get a dropbox account which is free. Then set up the API for python https://www.dropbox.com/developers/documentation/python#tutorial This includes downloading the sdk and registering the app to access the API.

When you've got a key enter that in the globalConfig.json file. More info on the system can be found on my blog https://danicymru.wordpress.com/2017/10/23/motion-detection-security-camera-using-pizero-and-opencv/ If you find anything of interest or any questions please put a comment on the blog.