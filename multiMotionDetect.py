#!/usr/bin/env python
#-------------------------------------------------------------------------------
# Name:        MultiProcessing Motion Detect System
#              
# Purpose:     System to record video of movement for use with security camera
#              
#
# Author:      Dani Thomas
#
# Requires:    OpenCV,json
# Based on:    
#-------------------------------------------------------------------------------
import cv2
import mialogging #  this is my customised logging class
from datetime import datetime
import time
import multiprocessing
import Queue
import signal
import os
import json

class WebCam:

  fourcc = cv2.VideoWriter_fourcc(*'XVID')
  
  rProcess=None
  wProcess=None
  danQueue=None
  ParentConn=None
  ChildConn=None
  
  def __init__(self, src=0):
   self.stopped = False
   currentDirectory=self.__getCurrentDirectory()
   logging.info("Current Directory: " + str(currentDirectory))
   self.GlobalConfigFile=self.__tryToLoadJsonFile(currentDirectory + 'globalConfig.json')
   #Save video root in global config as we need it across programs
   REMOTE_CONFIG_PATH = self.GlobalConfigFile['VIDEO_ROOT'] + self.GlobalConfigFile['REMOTE_CONFIG_FILE']
   self.RemoteConfigFile=self.__tryToLoadJsonFile(REMOTE_CONFIG_PATH)
   # initialize the video camera stream and read the first frame
   # from the stream
   self.stream = cv2.VideoCapture(src)
   self.stream.set(int(self.GlobalConfigFile['CAP_PROP_FRAME_WIDTH']), int(self.GlobalConfigFile['SMALL_CAMERA_WIDTH']))
   self.stream.set(int(self.GlobalConfigFile['CAP_PROP_FRAME_HEIGHT']), int(self.GlobalConfigFile['SMALL_CAMERA_HEIGHT']))
   # Create the queues and pip plus events to control the different processes
   self.frameQueue = multiprocessing.Queue()
   self.ChildConn,self.ParentConn  = multiprocessing.Pipe(duplex=False)
   self.writeEvent = multiprocessing.Event()
   self.stopEvent =  multiprocessing.Event()
   self.reqEvent =  multiprocessing.Event()
   # Create the processes Write Cam and Check motion. This is the readcam process
   self.wProcess = multiprocessing.Process(target=self.WriteCam, args=(self.frameQueue,self.writeEvent, self.stopEvent))
   self.chkProcess = multiprocessing.Process(target=self.CheckMotion, args=(self.ChildConn,self.writeEvent,self.reqEvent,self.stopEvent))
   self.wProcess.start()
   self.chkProcess.start()   
  
  def stop(self):
    # indicate that the system should be stopped. Stops the other processes
    logging.info("setting stop flag " + str(self.stopped))
    self.stopped= True
    logging.info("Closing parent connection") 
    self.ParentConn.close()
    logging.info("Closing child connection")
    self.ChildConn.close()
    logging.info("Setting stop event")   
    self.stopEvent.set()
    logging.info("waiting for check motion process ...")
    self.chkProcess.join()  
    logging.info("waiting for write process ...")
    self.wProcess.join()
  
  # Set the initial speed at the same time warm the camera up
  def setSpeed(self):
    fps=0.0
    secs=10
    self.startTime=datetime.now()
    while ((datetime.now() - self.startTime).total_seconds() <= secs):
      self.stream.read()
      fps=fps+1
    return(round(fps / secs))

  def start(self):
    # set the priority of this process
    if mialogging.username=='root':
      os.nice(int(self.RemoteConfigFile['readCamNice']))
    fps=self.setSpeed()
    frames=0
    frameUnit=1200
    logging.info ("Initial fps: " + str(fps))
    # Pass the frames per second to the video writer so that it can write the correct speed
    self.frameQueue.put(fps)
    # Loop reading frames from the camera
    while not(self.stopped):
      (grabbed, frame) = self.stream.read()
      if not(grabbed):
        break;
      if not(self.stopped):
        # Put frame on queue so it can be written if required
        self.frameQueue.put(frame)
        # The check motion makes a request for a frame otherwise don't get held up
        if self.reqEvent.is_set():
          self.ParentConn.send(frame)
          self.reqEvent.clear()
        frames=frames+1
        # From time to time to time we need to revise the frames per second 
        # because of changes in the light
        if frames == frameUnit:
          self.startTime=datetime.now()
        elif frames== (frameUnit * 2):
          newfps=frameUnit//(datetime.now()-self.startTime).total_seconds()
          if newfps<>fps:
            fps=newfps
            logging.info("New fps: " + str(fps))
            # Occasionally the fps goes haywire due to some issue reading from webcam
            if fps > 40:
              logging.error("fps has gone haywire")
              cv2.imwrite("/home/pi/failed.jpg",frame)
              break;
            self.frameQueue.put(fps)
          frames=0
    
    # If there is a an error try and quit gracefully
    if not(self.stopped):
      self.stop()
    logging.info ("stopping stream")
    self.stream.release()
    logging.info("closing queue")
    self.frameQueue.close()  
    logging.info ("closing cv2")
    
    cv2.destroyAllWindows()
    logging.info ("start cam finished")
  
  
  # This is part of the Check Motion process
  # Check if the motion is totally contained within a masked area
  def ContainedInMaskAreas(self,x,y,w,h,MaskedAreas,frame):
    for maskedArea in MaskedAreas["maskedAreas"]:
       cv2.rectangle(frame, (int(maskedArea["x"]), int(maskedArea["y"])), (int(maskedArea["w"]), int(maskedArea["h"])), (255, 0, 0), 1)
       if x < int(maskedArea["x"]):
         continue
       if y < int(maskedArea["y"]):
         continue
       if w > int(maskedArea["w"]):
         continue
       if h > int(maskedArea["h"]):
         continue
       return True
    return False

  # Check Motion process
  def CheckMotion(self,ChildConn,writeEvent,reqEvent,stopEvent):
    # set the priority of the process 
    os.nice(int(self.RemoteConfigFile['checkMotionNice']))
    # The number of movement frames to say it is significant
    framethreshold = int(self.RemoteConfigFile['frameThreshold'])
    # The number of non movement frames to count movement has ended
    staticthreshold = int(self.RemoteConfigFile['staticThreshold'])
    # The minimum size of movement to consider significant
    min_area = int(self.RemoteConfigFile['min_area'])
    changedFrames=0
    staticFrames=0
    delta_thresh = 5
    # load masked areas from file so we can change remotely
    MASKED_NAME = self.GlobalConfigFile['VIDEO_ROOT'] + self.GlobalConfigFile['MASKED_FILE']
    MaskedAreas=self.__tryToLoadJsonFile(MASKED_NAME)
    time.sleep(10)
    # Request a frame from the read camera process
    reqEvent.set()
    while reqEvent.is_set():
      pass    
    try:
      frame=ChildConn.recv()
    
      gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
      gray = cv2.GaussianBlur(gray, (21, 21), 0)
    
      logging.info("starting avg (background)")
      avg = gray.copy().astype("float")
      reqEvent.set()

    
      while not(stopEvent.is_set()):
        while reqEvent.is_set():
          if stopEvent.is_set():
            raise ValueError('stop')
        frame=ChildConn.recv()
        # get a monochrome version of frame
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        # blur to fill in small gaps
        gray = cv2.GaussianBlur(gray, (21, 21), 0)
        # accumulate the weighted average between the current frame and
        cv2.accumulateWeighted(gray, avg, 0.5)
        frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))
        # threshold the delta image, dilate the thresholded image to fill
        # in holes, then find contours on thresholded image
        thresh = cv2.threshold(frameDelta,delta_thresh,255,cv2.THRESH_BINARY)[1]
        thresh = cv2.dilate(thresh, None, iterations=2)
        (_,cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
 
        bigMotion=False
        maskedMotion=False
        # loop over the contours
        for c in cnts:
 		     # if the contour is too small, ignore it
           cntArea=cv2.contourArea(c)
           if cntArea < min_area:
 			        continue
           else:
             (x, y, w, h) = cv2.boundingRect(c)
           # this is to get rid of refresh issues and camera refocussing. If fills the whole frame
           if w>=int(self.GlobalConfigFile['SMALL_CAMERA_WIDTH'])-4:
             continue
           # Totally contained within masked area
           if self.ContainedInMaskAreas(x,y,x + w,y + h,MaskedAreas,frame):
             maskedMotion=True
             logging.debug("blue frame")
             continue 
           #cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 1)
           # Here we have motion outside the masked areas so is significant
           logging.debug("green frame( {0},{1},{2},{3} )".format(x,y,x+w,y+h))         
           bigMotion=True
        
        # write event set means we are actually filming
        if writeEvent.is_set():
          # Check if motion has finished
          if not(bigMotion or maskedMotion):
            staticFrames=staticFrames+1
            if staticFrames % 20 ==0:
              logging.debug("static frames {0}".format(staticFrames)) 
            if staticFrames >= staticthreshold:
              writeEvent.clear()
              # Be ready to restart filming if movement starts again
              # Closing file and rewriting a new one uses resources.
              changedFrames=framethreshold-1
              staticFrames=0
          else:
            staticFrames=0
            changedFrames=changedFrames+1  
        else:
          # Not filming yet
          if maskedMotion or bigMotion:
            staticFrames=0
            changedFrames=changedFrames+1
          elif changedFrames > 0:
            logging.debug("no movement")
            changedFrames=changedFrames - 1
          if bigMotion:
            # Enough to start writing the film
            if changedFrames >= framethreshold:
              writeEvent.set()
        # Request next frame
        reqEvent.set()

         
    except Exception as e:
      if str(e)<> 'stop':
        logging.error( "Exception was " + str(e) ) 
    finally:
      logging.info("closing checkMotion")
  
  def __getCurrentDirectory(self):
    return os.path.dirname(os.path.realpath(__file__))+'/'
    
  # Get and load Json file
  def __tryToLoadJsonFile( self , jsonFilePath):  
    if os.path.exists( jsonFilePath ):
        try:
            with open( jsonFilePath ) as jsonFile:
                
                return json.load( jsonFile )
                           
        except Exception as e:
            logging.error( "Unable to load file. Exception was " + str( e ) ) 
    else:
        logging.error( "Unable to locate file: " + jsonFilePath)
       
  # This is part of the write camera process 
  def endFilming(self,video):
    if video<>None:
      video.release()
      logging.info("stop filming")
      logging.info("------------")
              
  def WriteCam(self,frameQueue,writeEvent,stopEvent):
    # Set the process priority
    os.nice(int(self.RemoteConfigFile['writeCamNice']))
    VIDEO_DIR=str(self.GlobalConfigFile['VIDEO_ROOT'] + self.GlobalConfigFile['VIDEO_DIR'])
    VIDEO_NAME=str(self.GlobalConfigFile['VIDEO_NAME'])
    maxqsize=int(self.RemoteConfigFile['maxqsize'])
    PostFramesDelay=0
    video=None
    filming=False
    fps = 4.0 # nominal frame per second rate
    errorcount=0
    
    while True:
      if stopEvent.is_set():
        mQsize = 0
      else:
        # Qsize is in number of seconds times the frames per second
        # To account for different light conditions 
        mQsize= maxqsize * int(fps)
      if filming:
        try:
          # get frame or fps from queue
          qItem=frameQueue.get(True,4)
        except Queue.Empty as e:
          if stopEvent.is_set():
            logging.info("write cam finished naturally")
            break
          else:
            logging.error ('Queue is empty exception while filming:' + e.__class__.__name__)
            if errorcount > 10:
              self.endFilming(video)
              filming=False
              stopEvent.set()
              break
            else:
              errorcount=errorcount+1
              continue
        # We get both the frames per second and the frames to write
        if type(qItem)==float:
          fps=qItem
        else:
          video.write(qItem)
        # We have been told that filming is to stop. But delay for a certain time
        if not(writeEvent.is_set()):
          if PostFramesDelayInd<1:
            self.endFilming(video)
            filming=False
          else:
            PostFramesDelayInd=PostFramesDelayInd-1
        else:
          PostFramesDelayInd=PostFramesDelay
      else:
        # We have been told to start filming so create file
        if writeEvent.is_set():
            errorcount=0
            start=datetime.now()  
            videoPath=str.format(VIDEO_DIR, start.strftime("%Y-%m-%d"))
            videoName=str.format(VIDEO_NAME,start.strftime("%H-%M-%S-%f"))
            if not os.path.exists(videoPath):
              os.makedirs(videoPath)
            video=cv2.VideoWriter(os.path.join(videoPath,videoName), self.fourcc, fps,(int(self.GlobalConfigFile['SMALL_CAMERA_WIDTH']),int(self.GlobalConfigFile['SMALL_CAMERA_HEIGHT'])))
            logging.info("------------")
            logging.info("start filming " + str(videoPath)+"/"+str(videoName))
            PostFramesDelay=int(self.RemoteConfigFile['postSeconds']) * fps
            PostFramesDelayInd = PostFramesDelay
            logging.debug("PostFramesDelay="+str(PostFramesDelay))
            filming=True
        # Otherwise just take frame off queue and do nothing if max queue size has been reached
        elif frameQueue.qsize() >= mQsize:           
          try:
            qItem=frameQueue.get(True,4)
          except Queue.Empty as e:
            if stopEvent.is_set():
              logging.info("write cam finished naturally")
              break
            else:
              logging.error ('Queue is empty exception while discarding:' + e.__class__.__name__)
    
          if type(qItem)==float:
            fps=qItem             
      
if __name__ == '__main__':
          
    logging=mialogging.logger
    logging.info("Starting Motion Detect")
    
    # Set signal handling of SIGINT to ignore mode.
    signal.signal(signal.SIGINT, signal.SIG_IGN)    
    cam = WebCam(src=0)
    
    def quitGracefully(*args):
      logging.info("quitting gracefully")
      cam.stop()
    
    # If control c is pressed should quit everything gracefully
    signal.signal(signal.SIGINT, quitGracefully)   
    
    cam.start()
    logging.info("Ending here")
    logging.info("--------------------------------------------------------------")