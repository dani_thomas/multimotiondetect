#-------------------------------------------------------------------------------
# Name:        miaLogReceiver
#              
# Purpose:     This is a socket handler so that logging can be implemented
#              from numerous sources. It rotates the log file at midnight and
#              keeps 5 backups
#
# Author:      Dani Thomas
#
# Requires:    
# Based on:    
#-------------------------------------------------------------------------------
import pickle
import logging
import logging.handlers
from logging.handlers import TimedRotatingFileHandler
import SocketServer
import struct
import os
import json

class LogRecordStreamHandler(SocketServer.StreamRequestHandler):
    """Handler for a streaming logging request.

    This basically logs the record using whatever logging policy is
    configured locally.
    """

    def handle(self):
        """
        Handle multiple requests - each expected to be a 4-byte length,
        followed by the LogRecord in pickle format. Logs the record
        according to whatever policy is configured locally.
        """
        while True:
            chunk = self.connection.recv(4)
            if len(chunk) < 4:
                break
            slen = struct.unpack('>L', chunk)[0]
            chunk = self.connection.recv(slen)
            while len(chunk) < slen:
                chunk = chunk + self.connection.recv(slen - len(chunk))
            obj = self.unPickle(chunk)
            record = logging.makeLogRecord(obj)
            self.handleLogRecord(record)

    def unPickle(self, data):
        return pickle.loads(data)

    def handleLogRecord(self, record):
        # if a name is specified, we use the named logger rather than the one
        # implied by the record.
        if self.server.logname is not None:
            name = self.server.logname
        else:
            name = record.name
        logger = logging.getLogger(name)
        # N.B. EVERY record gets logged. This is because Logger.handle
        # is normally called AFTER logger-level filtering. If you want
        # to do filtering, do it at the client end to save wasting
        # cycles and network bandwidth!
        logger.handle(record)

class LogRecordSocketReceiver(SocketServer.ThreadingTCPServer):
    """
    Simple TCP socket-based logging receiver suitable for testing.
    """

    allow_reuse_address = 1

    def __init__(self, host='localhost',
                 port=logging.handlers.DEFAULT_TCP_LOGGING_PORT,
                 handler=LogRecordStreamHandler):
        SocketServer.ThreadingTCPServer.__init__(self, (host, port), handler)
        self.abort = 0
        self.timeout = 1
        self.logname = None

    def serve_until_stopped(self):
        import select
        abort = 0
        while not abort:
            rd, wr, ex = select.select([self.socket.fileno()],
                                       [], [],
                                       self.timeout)
            if rd:
                self.handle_request()
            abort = self.abort

def main():

    # Get the directory this script comes from
    def getCurrentDirectory():
      return os.path.dirname(os.path.realpath(__file__))+'/'
    
    # Get and load Json file
    def tryToLoadJsonFile(jsonFilePath):  
      if os.path.exists( jsonFilePath ):
        try:
            with open( jsonFilePath ) as jsonFile:
                
                return json.load( jsonFile )
                           
        except Exception as e:
            logging.error( "Unable to load file. Exception was " + str( e ) ) 
      else:
        logging.error( "Unable to locate file: " + jsonFilePath)
        
    # Get global config file for paths etc      
    HOME_DIR=getCurrentDirectory()
    GlobalConfigFile=tryToLoadJsonFile(HOME_DIR + 'globalConfig.json')
    LOG_FILE=str(GlobalConfigFile['LOG_FILE'])
    VIDEO_ROOT=str(GlobalConfigFile['VIDEO_ROOT'])
    
    #set logfile to the video folder so we can access remotely
    logfile=VIDEO_ROOT+LOG_FILE
    logger = logging.getLogger('miaMotionDetectLog')
    logger.setLevel(logging.INFO) 
    
    handler = TimedRotatingFileHandler(logfile,
                                       when="midnight",
                                       interval=1,
                                       backupCount=5
                                       )

    formatter = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s','%Y-%m-%d %H:%M:%S')                     
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    tcpserver = LogRecordSocketReceiver()
    tcpserver.serve_until_stopped()

if __name__ == '__main__':
    main()