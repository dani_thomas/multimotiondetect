#-------------------------------------------------------------------------------
# Name:        miaLogger
#              
# Purpose:     Class to log errors from motion detect python programs as well as
#              standard error. If the user is pi then display to standard output
#              as well
#
# Author:      Dani Thomas
#
# Requires:    OpenCV
# Based on:    
#-------------------------------------------------------------------------------
import logging, logging.handlers
import sys
import getpass

class miaLogger(object):
   """
   Fake file-like stream object that redirects writes to a logger instance.
   """
   def __init__(self, logger, log_level=logging.INFO):
      self.logger = logger
      self.log_level = log_level
      self.linebuf = ''
 
   def write(self, buf):
      for line in buf.rstrip().splitlines():
         self.logger.log(self.log_level, line.rstrip())
   
   def flush(self): 
    pass

logger = logging.getLogger('miaMotionDetectLog')
logger.setLevel(logging.INFO)
socketHandler = logging.handlers.SocketHandler('localhost',
                    logging.handlers.DEFAULT_TCP_LOGGING_PORT)
                    
# don't bother with a formatter, since a socket handler sends the event as
# an unformatted pickle
logger.addHandler(socketHandler)

sl = miaLogger(logger, logging.ERROR)
sys.stderr = sl

username=getpass.getuser()

# Also log to stdout
if username=='pi':
  consoleHandler = logging.StreamHandler(sys.stdout)
  consoleHandler.setLevel( logging.DEBUG )
  logger.addHandler( consoleHandler )