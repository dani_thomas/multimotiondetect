#-------------------------------------------------------------------------------
# Name:        Mia Process
#              
# Purpose:     System to check on processes
#              
#
# Author:      Dani Thomas
#
# Requires:    
# Based on:    
#-------------------------------------------------------------------------------
import subprocess
import os

class MiaProcess():
    
  # Match processes to filename to check whether running 
  def getProcessesRunning(self,filename):
    ps = subprocess.Popen("pgrep -f " + str(filename) + "| wc -w", shell=True, stdout=subprocess.PIPE)
    output = ps.stdout.read()
    ps.stdout.close()
    ps.wait()
    return int(output)-1
  
  # Checks whether process already running. Greater than 2 is required as 1st prcoess is the calling program
  # 2nd is the command checking the program.
  def amIrunning(self,filename):
    if self.getProcessesRunning(filename) > 2:
      return True
    else:
      return False
  
      